#Journal

## 31/8/2017

Rhiannon and I discussed ideas for our application. Rejected ideas included an amalgamated Social media app, allowing people to update multiple platforms from one point. In the end it was decided to make an app about finding nearby places to eat, as we were both hungry.
Upon research, we discovered wtfsigte.com. we thought it was a good idea, but having that functionality without the language would be great. We decided to use google maps and places to create a search engine to find places to eat. 

## 6/9/2017

We discussed tools we would be using. Most of it was chosen for us by our module. Electron is the framework, Visual studio code is the development platform, javascript, html, and CSS are the languages. The marking schedule mentioned having clear team allocations, so naturally I nominated Rhiannon as Team Leader. It was determined that we would work together to implement the API's, as it was a key learning oppurtunity. In addition, Rhiannon would be lead on any additional JS functionality, while I took lead on the front end HTML/CSS using bootstrap.

## 7/9/2017 

Rhiannon and I sat down and discussed the layout of the wireframes. She drew, and I gave feedback and ideas. We didn't want to venture to far from the proven design of google maps, but needed to make allowances for our code.

## 8/9/2017

I put together the Tools and Resources section, exploring and finding links to all the resources we will be using and referencing during this project. 

## 9/9/2017

Rhiannon and I met and amalgamated our sections of the report into the final draft. 

## 11/11/2017

Rhiannon and I put the wireframes into png format and implemented them into the report. We worked together to proofread and submit the project.